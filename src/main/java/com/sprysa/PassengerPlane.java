package com.sprysa;

public class PassengerPlane extends Plane {

  private int passengerCapacity;

  public PassengerPlane(int flightDistance, int passengerCapacity) {
    super(flightDistance);
    this.passengerCapacity = passengerCapacity;
  }

  public int getCapacity() {
    return passengerCapacity;
  }
}

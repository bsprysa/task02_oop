package com.sprysa;

public class Plane {

  private int flightDistance;
  private int fuelConsumption;

  public Plane(int flightDistance) {
    this.flightDistance = flightDistance;
  }

  public int getFlightDistance() {
    return flightDistance;
  }

  public int getFuelConsumption() {
    return fuelConsumption;
  }
}

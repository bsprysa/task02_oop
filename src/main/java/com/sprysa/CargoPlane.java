package com.sprysa;

public class CargoPlane extends Plane {

  private int loadCapacity;

  public CargoPlane(int flightDistance, int loadCapacity) {
    super(flightDistance);
    this.loadCapacity = loadCapacity;
  }

  public int getCapacity() {
    return loadCapacity;
  }

}

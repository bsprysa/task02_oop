package com.sprysa;

import java.util.ArrayList;

public class Airline {

  private ArrayList<PassengerPlane> passengerPlanes = new ArrayList();
  private ArrayList<CargoPlane> cargoPlanes = new ArrayList();

  public Airline(ArrayList<PassengerPlane> passengerPlanes, ArrayList<CargoPlane> cargoPlanes) {
    this.passengerPlanes = passengerPlanes;
    this.cargoPlanes = cargoPlanes;
  }

  public ArrayList<PassengerPlane> sortPassengerPlanesByFlightDistance() {
    passengerPlanes.sort(
        (PassengerPlane o1, PassengerPlane o2) -> o1.getFlightDistance() - o2.getFlightDistance());
    return passengerPlanes;
  }

  public ArrayList<CargoPlane> sortCargoPlanesByFlightDistance() {
    cargoPlanes.sort(
        (CargoPlane o1, CargoPlane o2) -> o1.getFlightDistance() - o2.getFlightDistance());
    return cargoPlanes;
  }

  public PassengerPlane getPassengerPlaneByRightFuelConsumption(int beginInterval,
      int endInterval) {
    for (PassengerPlane p : passengerPlanes) {
      if (p.getFuelConsumption() >= beginInterval && p.getFuelConsumption() <= endInterval) {
        return p;
      }
    }
    return null;
  }

  public CargoPlane getCargoPlaneByRightFuelConsumption(int beginInterval, int endInterval) {
    for (CargoPlane p : cargoPlanes) {
      if (p.getFuelConsumption() >= beginInterval && p.getFuelConsumption() <= endInterval) {
        return p;
      }
    }
    return null;
  }
}
